import "./App.css";
import React from "react";
import axios from "axios";
import { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import logo from "./logo22.png";
import { MdLogout, MdOutlineHome, MdOutlineFileUpload } from "react-icons/md";

const baseURL = "https://lol-drop.herokuapp.com/";

function Profile() {
  let navigate = useNavigate();
  const [name, setName] = useState("");
  const [password, setPassword] = useState("");
  const [password1, setPassword1] = useState("");
  const [password2, setPassword2] = useState("");

  // States for checking the errors
  const [submitted, setSubmitted] = useState(false);
  const [error, setError] = useState(false);
  const [response, setResponse] = useState("");
  const [errors, setErrors] = useState([]);

  const handleName = (e) => {
    setName(e.target.value);
    setSubmitted(false);

    document.getElementById("username").style.border = "1px solid #37C8AB";
  };

  // Handling the password change
  const handlePassword = (e) => {
    setPassword(e.target.value);
    setSubmitted(false);
  };
  const handlePassword1 = (e) => {
    setPassword1(e.target.value);
    setSubmitted(false);
    document.getElementById("newPassword1").style.border = "1px solid #37C8AB";
  };

  const handlePassword2 = (e) => {
    setPassword2(e.target.value);
    setSubmitted(false);
    document.getElementById("newPassword2").style.border = "1px solid #37C8AB";
  };


  const home = () => {
    let path = `/homePage`;
    navigate(path);
  };

  const imageUpload = () => {
    let path = `/image`;
    navigate(path);
  };

  const logOut = () => {
    axios.defaults.headers.common[
      "Authorization"
    ] = `Token ${sessionStorage.getItem("user_token")}`;
    axios.post(baseURL + "rest-auth/logout/").then((response) => {
      console.log(response);
      delete axios.defaults.headers.common["Authorization"];
      sessionStorage.removeItem("user_token");
      login();
    });
  };

  const login = () => {
    let path = `/login`;
    navigate(path);
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    console.log({
      username: password1,
    });
    axios.defaults.headers.common[
      "Authorization"
    ] = `Token ${sessionStorage.getItem("user_token")}`;
    axios
      .patch(baseURL + "rest-auth/user/", {
        username: name,
      })
      .then((response) => {
        setResponse(response.statusText);
        setSubmitted(true);
        setError(false);
      })
      .catch((error) => {
        var errors = [];
        var result = error.response.data;
        console.log("JSON error ->", result);

        if (result.username) {
          setError(true);

          document.getElementById("username").style.border =
            "1px solid #830000";
          errors.push(JSON.stringify(result.username));
          console.log(
            "json response username:" + JSON.stringify(result.username)
          );
        }
        if (result.email) {
          setError(true);
          errors.push(JSON.stringify(result.email));
          console.log("json response email:" + JSON.stringify(result.email));
        }
        if (result.non_field_errors) {
          setError(true);
          errors.push(JSON.stringify(result.non_field_errors));
          console.log(
            "json response other:" + JSON.stringify(result.non_field_errors)
          );
        }

        setErrors(errors);
        console.log("error ->", error);
      });
  };

  // Handling the form submission
  const handlePasswordChange = (e) => {
    e.preventDefault();

    console.log({
      new_password1: password1,
      new_password2: password2,
      old_password: password,
    });
    axios.defaults.headers.common[
      "Authorization"
    ] = `Token ${sessionStorage.getItem("user_token")}`;
    axios
      .post(baseURL + "rest-auth/password/change/", {
        new_password1: password1,
        new_password2: password2,
        old_password: password,
      })
      .then((response) => {
        setResponse(response.statusText);
        setSubmitted(true);
        setError(false);
      })
      .catch((error) => {
        var errors = [];
        var result = error.response.data;
        console.log("JSON error ->", result);
        if (result.new_password1) {
          setError(true);
          document.getElementById("newPassword1").style.border =
            "1px solid #830000";
          errors.push(JSON.stringify(result.new_password1));
          console.log(
            "json response password:" + JSON.stringify(result.new_password1)
          );
        }
        if (result.new_password2) {
          setError(true);
          document.getElementById("newPassword2").style.border =
            "1px solid #830000";
          errors.push(JSON.stringify(result.new_password2));
          console.log(
            "json response password:" + JSON.stringify(result.new_password2)
          );
        }

        if (result.email) {
          setError(true);
          errors.push(JSON.stringify(result.email));
          console.log("json response email:" + JSON.stringify(result.email));
        }
        if (result.non_field_errors) {
          setError(true);
          errors.push(JSON.stringify(result.non_field_errors));
          console.log(
            "json response other:" + JSON.stringify(result.non_field_errors)
          );
        }

        setErrors(errors);
        console.log("error ->", error);
      });
  };

  // Showing success message
  const successMessage = () => {
    var result = response;
    //console.log(result)

    return (
      <div
        className="success"
        style={{
          display: submitted ? "" : "none",
        }}
      >
        <h3>User {result === "OK" ? "saved" : "not saved"}</h3>
      </div>
    );
  };

  // Showing error message if error is true
  const errorMessage = () => {
    var results = [];
    for (var i = 0; i < errors.length; i++) {
      var temp = errors[i].toString();
      results.push(<p>{temp.substring(2, temp.length - 2)}</p>);
    }
    return (
      <div
        className="error"
        style={{
          display: error ? "" : "none",
        }}
      >
        {results}
      </div>
    );
  };

  const load = async () => {
    if (!sessionStorage.getItem("user_token")) {
      login();
    }
  };

  useEffect(() => {
    let abortController = new AbortController();
    load();
    return () => {
      abortController.abort();
    };
  });

  return (
    <div>
      <div style={{ position: "relative" }} className="TopBar">
      <div className="navBarNoCollapse">
        <img src={logo} alt="Logo" className="Logo" />
        <nav>
          <button className="btnupload2" onClick={home}>
            <MdOutlineHome />
          </button>
          <button href="" className="noClick">
            |
          </button>
          <button className="btnupload2" href="" onClick={imageUpload}>
            <MdOutlineFileUpload />
          </button>
        </nav>
        <button className="profile">
          {sessionStorage
            .getItem("user_name")
            .charAt(0)
            .toUpperCase()}
        </button>
        <button className="logout" onClick={logOut}>
          <MdLogout />
        </button>
      </div>
        <div className="navBarCollapsed">
          <img src={logo} alt="Logo" className="LogoCollapsed" />
          <nav>
          <button className="menuCollapsed" onClick={home}>
            <MdOutlineHome />
          </button>
          
          
          <button className="logoutCollapsed" onClick={logOut}>
          <MdLogout />
          </button>
          
        
          </nav>
        </div>
      </div>
      <div className="profileDiv">
        <div className="messages">
          {errorMessage()}
          {successMessage()}
        </div>

        <form>
          <ul>
            <li>
              <p className="profile-image">
                {sessionStorage
                  .getItem("user_name")
                  .charAt(0)
                  .toUpperCase()}
              </p>
            </li>

            <li>
              <div className="input_div">
                <label>Name</label>
                <input
                  id="username"
                  placeholder={sessionStorage.getItem("user_name")}
                  onChange={handleName}
                  className="profile-input"
                  value={name}
                  type="text"
                />
              </div>
            </li>

            <li>
              <button onClick={handleSubmit} className="btn" type="submit">
                Save
              </button>
            </li>
          </ul>
          <ul>
            <li>
              <div className="input_div">
                <label>Current password</label>
                <input
                  id="oldPassword"
                  autocomplete="new-password"
                  onChange={handlePassword}
                  className="profile-input"
                  value={password}
                  type="password"
                />
              </div>
            </li>
            <li>
              <div className="input_div">
                <label>New password</label>
                <input
                  id="newPassword1"
                  autocomplete="new-password"
                  onChange={handlePassword1}
                  className="profile-input"
                  value={password1}
                  type="password"
                />
              </div>
            </li>
            <li>
              <div className="input_div">
                <label>Confirm new password</label>
                <input
                  id="newPassword2"
                  autocomplete="new-password"
                  onChange={handlePassword2}
                  className="profile-input"
                  value={password2}
                  type="password"
                />
              </div>
            </li>
            <li>
              <button
                onClick={handlePasswordChange}
                className="btn"
                type="submit"
              >
                Change password
              </button>
            </li>
          </ul>
        </form>
      </div>
    </div>
  );
}

export default Profile;
