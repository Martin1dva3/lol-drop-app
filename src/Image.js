import "./App.css";
import React, { useCallback } from "react";
import axios from "axios";
import { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import logo from "./logo22.png";
import Dropzone, { useDropzone } from "react-dropzone";
import { MdLogout, MdOutlineHome } from "react-icons/md";

const baseURL = "https://lol-drop.herokuapp.com/";

function Image() {
  let navigate = useNavigate();
  const home = () => {
    let path = `/homePage`;
    navigate(path);
  };

  const [uploading, setUploding] = useState(false);
  const [files, setFiles] = useState([]);
  const [imageSelected, setImageSelected] = useState("");
  const [template, setTemplate] = useState("false");
  const [isTemplate, setIsTemplate] = useState(false);
  var urladresa = "";

  const logOut = () => {
    axios.defaults.headers.common[
      "Authorization"
    ] = `Token ${sessionStorage.getItem("user_token")}`;
    axios.post(baseURL + "rest-auth/logout/").then((response) => {
      //console.log(response);
      delete axios.defaults.headers.common["Authorization"];
      login();
    });
  };

  const login = useCallback(() => {
    let path = `/login`;
    navigate(path);
  },[navigate]);

  const load = useCallback(() => {
    if (!sessionStorage.getItem("user_token")) {
      login();
    }
  },[login]);

  const { getRootProps, getInputProps } = useDropzone({
    onClick: (acceptedFiles) => {
      setFiles(
        acceptedFiles.map((file) =>
          Object.assign(file, {
            preview: URL.createObjectURL(file),
          })
        )
      );
      setImageSelected(acceptedFiles[0]);
    },
    onDrop: (acceptedFiles) => {
      setFiles(
        acceptedFiles.map((file) =>
          Object.assign(file, {
            preview: URL.createObjectURL(file),
          })
        )
      );
      setImageSelected(acceptedFiles[0]);
    },
    type: "file",
  });

  const images = files.map((file) => (
    <div key={file.name}>
      <div>
        <img src={file.preview} style={{ width: "200px" }} alt="preview" />
      </div>
    </div>
  ));

  const handleInputChange = (event) => {
    setImageSelected(event.target.files[0]);
  };

  const handleRadioChange = (event) => {
    //console.log(event.target.value);
    setTemplate(event.target.value);
    if (event.target.value === "true") {
      setIsTemplate(true);
      //console.log(isTemplate);
    } else {
      setIsTemplate(false);
      //console.log(isTemplate);
    }
  };

  const afterUpload = () => {
    axios.defaults.headers.common[
      "Authorization"
    ] = `Token ${sessionStorage.getItem("user_token")}`;
    axios
      .post("https://lol-drop.herokuapp.com/api/posts/", {
        date: new Date(),
        media: urladresa,
        views: 0,
        template: isTemplate,
        ownerid: sessionStorage.getItem("user_pk"),
        owner: sessionStorage.getItem("user_name"),
      })
      .then((response) => {
        //console.log(response.data);
        if (response.data.created) {
          home();
        }
      })
      .catch((error) => {
        console.log(error.response.data);
      });
  };

  const uploadImage = () => {
    
    setUploding(true);
    const formData = new FormData();
    formData.append("file", imageSelected);
    //console.log(formData);
   // console.log(imageSelected);
    axios
      .post("https://lol-drop-image.herokuapp.com/upload", formData)
      .then((response) => {

        urladresa = response.data.url;

        //console.log("url: " + urladresa);

      //  console.log(response.data.url);
        afterUpload();
      });
  };

  useEffect(() => {
    let abortController = new AbortController();
    load();
    return () => {
      abortController.abort();
    };
  }, [load]);

  return (
    <div>
      <div className="TopBar">
      <div className="navBarNoCollapse">
        <img src={logo} alt="Logo" className="Logo" />
        <nav>
          <button className="btnupload2" onClick={home}>
            <MdOutlineHome />
          </button>
        </nav>
        <button className="logout" onClick={logOut}>
          <MdLogout />
        </button>
        </div>
        <div className="navBarCollapsed">
          <img src={logo} alt="Logo" className="LogoCollapsed" />
          <nav>
          <button className="menuCollapsed" onClick={home}>
            <MdOutlineHome />
          </button>
          
          <button className="logoutCollapsed" onClick={logOut}>
          <MdLogout />
        </button>
        
          </nav>
        </div>
      </div>
      
      <div className="Upload">
      <div
        className="successImage"
        style={{
          display: uploading ? "block" : "none",
        }}
      >
        <h3>Uploading ...</h3>
      </div>
        <br />
        <br />

        <br />
        <div>
          <div>
            <div {...getRootProps({ className: "dropzone" })}>
              <Dropzone
                accept="image/png, image/gif image/jpg"
                {...getInputProps()}
              >
                {(dropzoneProps) => {
                  return (
                    <div>
                      <p>Drop some files here</p>
                      {images}
                    </div>
                  );
                }}
              </Dropzone>
            </div>
            <form>
              <input
                type="file"
                className="file-input"
                name="upload_file"
                accept="image/*"
                onChange={handleInputChange}
              />
            </form>
          </div>
          <div className="wrapper">
            <input
              type="radio"
              value="true"
              name="isTamplate"
              id="option-1"
              checked={template === "true"}
              onChange={handleRadioChange}
            />
            <input
              type="radio"
              value="false"
              name="isTamplate"
              id="option-2"
              checked={template === "false"}
              onChange={handleRadioChange}
            />
            <label htmlFor="option-1" className="option option-1">
              <div className="dot"></div>
              <span>Template</span>
            </label>
            <label htmlFor="option-2" className="option option-2">
              <div className="dot"></div>
              <span>MEME</span>
            </label>
          </div>
        </div>

        <div className="form-row">
          <div className="col-md-6">
            <button
              type="submit"
              className="btn btn-dark"
              onClick={uploadImage}
            >
              Save
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Image;
