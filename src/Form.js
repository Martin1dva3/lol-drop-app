import { useState } from "react";
import React from "react";
import { useNavigate } from "react-router-dom";
import logo from "./logo22.png";
import axios from "axios";

//const baseURL = "http://127.0.0.1:8000/registration_api/users/";
const baseURL = "https://lol-drop.herokuapp.com/rest-auth/registration/";

export default function Form() {
  let navigate = useNavigate();
  const routeChange = () => {
    let path = `/login`;
    navigate(path);
  };
  // States for registration
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [password2, setPassword2] = useState("");

  // States for checking the errors
  const [submitted, setSubmitted] = useState(false);
  const [error, setError] = useState(false);
  const [response, setResponse] = useState("");
  const [errors, setErrors] = useState([]);

  // Handling the name change
  const handleName = (e) => {
    setName(e.target.value);
    setSubmitted(false);
    document.getElementById("username").style.border = "1px solid #37C8AB";
  };

  // Handling the email change
  const handleEmail = (e) => {
    setEmail(e.target.value);
    setSubmitted(false);
    document.getElementById("email").style.border = "1px solid #37C8AB";
  };

  // Handling the password change
  const handlePassword = (e) => {
    setPassword(e.target.value);
    setSubmitted(false);
    document.getElementById("password").style.border = "1px solid #37C8AB";
  };

  const handlePassword2 = (e) => {
    setPassword2(e.target.value);
    setSubmitted(false);
    document.getElementById("password2").style.border = "1px solid #37C8AB";
  };

  // Handling the form submission
  const handleSubmit = (e) => {
    e.preventDefault();

   // console.log({ email: email, name: name, password: password });

    delete axios.defaults.headers.common["Authorization"];
    axios
      .post(baseURL, {
        email: email,
        username: name,
        password1: password,
        password2: password2,
      })
      .then((response) => {
        setResponse(response.statusText);
        setSubmitted(true);
        setError(false);
        routeChange();
      })
      .catch((error) => {
        var errors = [];
        var result = error.response.data;
        console.log("JSON error ->", result);
        if (result.password1) {
          setError(true);
          document.getElementById("password").style.border =
            "1px solid #830000";
          errors.push(JSON.stringify(result.password1));
          console.log(
            "json response password:" + JSON.stringify(result.password1)
          );
        }
        if (result.password2) {
          setError(true);
          document.getElementById("password2").style.border =
            "1px solid #830000";
          errors.push(JSON.stringify(result.password2));
          console.log(
            "json response password:" + JSON.stringify(result.password2)
          );
        }
        if (result.username) {
          setError(true);
          document.getElementById("username").style.border =
            "1px solid #830000";
          errors.push(JSON.stringify(result.username));
          console.log(
            "json response username:" + JSON.stringify(result.username)
          );
        }
        if (result.email) {
          setError(true);
          document.getElementById("email").style.border = "1px solid #830000";
          errors.push(JSON.stringify(result.email));
          console.log("json response email:" + JSON.stringify(result.email));
        }
        if (result.non_field_errors) {
          setError(true);
          document.getElementById("password").style.border =
            "1px solid #830000";
          document.getElementById("password2").style.border =
            "1px solid #830000";
          errors.push(JSON.stringify(result.non_field_errors));
          console.log(
            "json response other:" + JSON.stringify(result.non_field_errors)
          );
        }

        setErrors(errors);
        console.log("error ->", error);
      });
  };

  // Showing success message
  const successMessage = () => {
    var result = JSON.stringify(response);

    return (
      <div
        className="success"
        style={{
          display: submitted ? "" : "none",
        }}
      >
        <h3>User {result === '"Created"' ? "created" : "not created"}</h3>
      </div>
    );
  };

  // Showing error message if error is true
  const errorMessage = () => {
    var results = [];
    for (var i = 0; i < errors.length; i++) {
      var temp = errors[i].toString();
      results.push(<p>{temp.substring(2, temp.length - 2)}</p>);
    }
    return (
      

      <div
       className="error"
        style={{
          display: error ? "" : "none",
        }}
      >
        {results}
      </div>
    );
  };

  return (
    <div>
    <img src={logo} alt="Logo" className="LogoLogin" />
    <div className="form">
      {/* <div>
      <img src={logo} alt="Logo" className='Logo'/>
      <h1>User Registration</h1>
    </div> */}

      {/* Calling to the methods */}
      <div className="messages">
        {errorMessage()}
        {successMessage()}
      </div>

      <form>
        {/* Labels and inputs for form data */}
        <div className="input_div">
          <label className="label">Name</label>
          <input
            id="username"
            onChange={handleName}
            className="input"
            value={name}
            type="text"
          />
        </div>
        <div className="input_div">
          <label className="label">Email</label>
          <input
            id="email"
            onChange={handleEmail}
            className="input"
            value={email}
            type="email"
          />
        </div>
        <div className="input_div">
          <label className="label">Password</label>
          <input
            id="password"
            onChange={handlePassword}
            className="input"
            value={password}
            type="password"
          />
        </div>
        <div className="input_div">
          <label className="label">Confirm Password</label>
          <input
            id="password2"
            onChange={handlePassword2}
            className="input"
            value={password2}
            type="password"
          />
        </div>
        <button onClick={handleSubmit} className="btn" type="submit">
          Submit
        </button>
      </form>
      <button onClick={routeChange} className="btn">
        Login
      </button>
    </div>
    </div>
  );
}
