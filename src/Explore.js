import "./App.css";
import React, { useEffect, useState } from "react";

import { useStateIfMounted } from "use-state-if-mounted";
import { useNavigate } from "react-router-dom";
import axios from "axios";
import logo from "./logo22.png";
import Moment from "moment";
import {
  MdMoreVert,
  MdDelete,
  MdDownload,
  MdThumbUp,
  MdThumbDown,
  MdLogout,
  MdOutlineFileUpload,
  MdOutlineHome,
} from "react-icons/md";

import {saveAs} from "file-saver";

function Explore() {
  const FileSaver = require('file-saver');
  const baseURL = "https://lol-drop.herokuapp.com/";
  let navigate = useNavigate();    
  const [modal,setModal]= useState(false);
  const [yesDelete, setYesDelete] = useState(false);
  const [isLoaded, setIsLoaded] = useState(false);
  const [toggle, setToggle] = useState(false);
  const [posts, setPosts] = useStateIfMounted([]);
  const [postArray, setPostArray] = useStateIfMounted([]);
  const [commentsArray, setCommentsArray] = useStateIfMounted([]);
  const [reactionsArray, setReactionsArray] = useStateIfMounted([]);
  const [name, setName] = useState("");
  const [comment, setComment] = useState("");

  var toDelete = 0;
  var postid = 0;
  var reactionToSend = 0;
  var reactionId = 0;

  const home = () => {
    let path = `/homePage`;
    navigate(path);
  };

  

  const imageUpload = () => {
    let path = `/image`;
    navigate(path);
  };

  const logOut = () => {
    axios.defaults.headers.common[
      "Authorization"
    ] = `Token ${sessionStorage.getItem("user_token")}`;
    axios.post(baseURL + "rest-auth/logout/").then((response) => {
      //console.log(response);
      delete axios.defaults.headers.common["Authorization"];
      login();
    });
  };

  const login = () => {
    let path = `/login`;
    navigate(path);
  };

  const loadExplore = async () => {
    
    if (!sessionStorage.getItem("user_token")) {
      login();
    }
  };

  const sendReaction = () => {
    //console.log("ahoj");
    /*console.log({
      date: new Date(),
      reaction: reactionToSend,
      postid: postid,
      ownerid: sessionStorage.getItem("user_pk"),
      owner: sessionStorage.getItem("user_name"),
    });*/

    axios.defaults.headers.common[
      "Authorization"
    ] = `Token ${sessionStorage.getItem("user_token")}`;
    axios
      .post(baseURL + "api/reactions/", {
        date: new Date(),
        reaction: reactionToSend,
        postid: postid,
        ownerid: sessionStorage.getItem("user_pk"),
        owner: sessionStorage.getItem("user_name"),
      })
      .then((response) => {
        //console.log(response);
        setComment("");
      })
      .catch((err) => {
        console.log(err.response.data);
      });
  };
  const updateReaction = () => {
    //console.log("ahoj");
    /*console.log({
      date: new Date(),
      reaction: reactionToSend,
      postid: postid,
      ownerid: sessionStorage.getItem("user_pk"),
      owner: sessionStorage.getItem("user_name"),
    });*/

    axios.defaults.headers.common[
      "Authorization"
    ] = `Token ${sessionStorage.getItem("user_token")}`;
    axios
      .patch(baseURL + `api/reactions/${reactionId}/`, {
        date: new Date(),
        reaction: reactionToSend,
        postid: postid,
        ownerid: sessionStorage.getItem("user_pk"),
        owner: sessionStorage.getItem("user_name"),
      })
      .then((response) => {
        //console.log(response);
      })
      .catch((err) => {
        console.log(err.response.data);
      });
  };

  const Delete = () => {    
    setModal(false);
    setIsLoaded(false);
    axios.defaults.headers.common[
      "Authorization"
    ] = `Token ${sessionStorage.getItem("user_token")}`;
    axios
      .delete(baseURL + `api/posts/${toDelete}`)
      .then((response) => {
        //console.log(response);
        setToggle(!toggle);
      })
      .catch((err) => {
        console.log(err.response);
      });
      setYesDelete(false);
  };


  const handleComment = (e) => {
    setComment(e.target.value);
    //console.log(comment);
  };

  const sendComment = (e) => {
    //console.log("ahoj");
   /* console.log({
      date: new Date(),
      text: comment,
      post: postid,
      ownerid: sessionStorage.getItem("user_pk"),
      owner: sessionStorage.getItem("user_name"),
    });*/

    axios.defaults.headers.common[
      "Authorization"
    ] = `Token ${sessionStorage.getItem("user_token")}`;
    axios
      .post(baseURL + "api/comments/", {
        date: new Date(),
        text: comment,
        post: postid,
        ownerid: sessionStorage.getItem("user_pk"),
        owner: sessionStorage.getItem("user_name"),
      })
      .then((response) => {
        //console.log(response);
        setComment("");
      })
      .catch((err) => {
        console.log(err.response.data);
      });
  };

  const handleName = (e) => {
    setName(e.target.value);
    getPosts();
  };

  const getPosts = () => {
    var posts = [];
    //console.log(postArray);
    if (name === "") {
      posts.push(
        <li key="empty meme list">
          <div className="PostEmpty">
            <div className="header">
              <h4>Type authors name</h4>
            </div>
          </div>
        </li>
      );
      setPosts(posts);
      return null;
    }

    postArray.forEach((post) => {
      //console.log(post.ownerid);
      var comms = [];

      commentsArray.forEach((com) => {
        //console.log(com);
        if (com.post === post.id) {
          comms.push(
            <li key={com.id}>
              <div>
                <div className="commentOwner">
                <b>
                  <p >{com.owner}</p>
                </b>
                </div>
                  <p className="commentText">{com.text}</p>                
              </div>
            </li>
          );
        }
      });
      //console.log(reactionsArray);
      var likes = "";
      var dislikes = "";
      var a = 0;
      var b = 0;
      
      var liked = [];
      var disliked = [];
      var reacted = [];
      var reactionID = 0;
      reactionsArray.forEach((reac) => {
        if (reac.postid === post.id) {
          
          if(!reacted.includes(reac.ownerid.toString())){
          reacted.push(reac.ownerid.toString());
          if (reac.reaction === 1) {            
          liked.push(reac.ownerid.toString());
            a = a + 1;
          } else {            
          disliked.push(reac.ownerid.toString());
            b = b + 1;
          }
          if (reac.ownerid.toString() === sessionStorage.getItem("user_pk")) {
            reactionID = reac.id;
            //console.log(reac.id)
          }
        }
        }

        //console.log(a);

        likes = a === 0 ? "" : a.toString();

        dislikes = b === 0 ? "" : b.toString();
      });

      if (post.owner.startsWith(name)) {
        // console.log(liked);
        posts.push(
          <li key={post.id}>
            <div className="Post">
              <div className="header">
                <div className="options">
                  <button className="optionsButton">
                    <MdMoreVert />
                  </button>
                  <div className="optionsDrop">
                    <button
                      style={{
                        display:
                          sessionStorage.getItem("user_pk") ===
                          post.ownerid.toString()
                            ? "block"
                            : "none",
                      }}
                      onClick={function() {
                        
                        toDelete = post.id;                        
                        setModal(true);
                        setYesDelete(true);
                        if(yesDelete){
                          Delete();
                        }
                      }}
                    >
                      <MdDelete />
                    </button>
                    <button
                      onClick={function() {
                        saveAs(post.media.toString());
                      }}
                    >
                      <MdDownload />
                    </button>
                  </div>
                </div>
                <h4>{post.owner}</h4>
                <p className="datum">
                  Posted: {Moment(post.date).format("DD.MM.YYYY")}
                </p>
              </div>
              <img src={post.media} alt={post.owner + " meme"}></img>
              <div
                style={{ display: post.template ? "none" : "block" }}
                className="commentSection"
              >
                
                <div className="reactions">
                <button
                  className={liked.includes(sessionStorage.getItem("user_pk"))?"reactionButton like liked":"reactionButton like"}
                  onClick={function() {
                    setToggle(!toggle);
                    postid = post.id;
                    reactionToSend = 1;
                    //console.log(sessionStorage.getItem("user_pk"));
                    if (!reacted.includes(sessionStorage.getItem("user_pk"))) {
                      sendReaction();
                    } else {
                      reactionId = reactionID;
                      updateReaction();
                      setToggle(!toggle);
                    }
                  }}
                >
                  <MdThumbUp />
                  {likes}
                </button>
                <button
                  className={disliked.includes(sessionStorage.getItem("user_pk"))?"reactionButton disLike disLiked":"reactionButton disLike"}
                  onClick={function() {
                    setToggle(!toggle);
                    postid = post.id;
                    reactionToSend = 0;
                    if (!reacted.includes(sessionStorage.getItem("user_pk"))) {
                      sendReaction();
                    } else {
                      reactionId = reactionID;
                      updateReaction();
                      setToggle(!toggle);
                    }
                  }}
                >
                  <MdThumbDown />
                  {dislikes}
                </button>
                </div>
                <div className="commentDrop">
                  <form >
                  <input
                    placeholder="Leave comment"
                    className="commentInput"
                    value={comment}
                    type="text"
                    onChange={handleComment}
                  />
                  <button
                  className="commentDropButton"
                    onClick={(e) =>{
                      setToggle(!toggle);
                      postid = post.id;
                      //console.log({ id: postid });
                      e.preventDefault();
                      sendComment();
                    }}
                    type="submit"
                  >
                    Send
                  </button>
                  </form>
                  <div className="comments">
                    <ul>{comms}</ul>
                  </div>
                </div>
              </div>
            </div>
          </li>
        );
      }
    });
    if (posts.length === 0) {
      posts.push(
        <li key="empty meme list">
          <div className="PostEmpty">
            <div className="header">
              <h4>This list is empty</h4>
            </div>
          </div>
        </li>
      );
    }
    setPosts(posts);
  };

  const fetchData = () => {
    axios.defaults.headers.common[
      "Authorization"
    ] = `Token ${sessionStorage.getItem("user_token")}`;
    let endpoints = [
      baseURL + "api/posts/",
      baseURL + "api/comments/",
      baseURL + "api/reactions/"
    ];
    //console.log(postArray);
    axios.all(endpoints.map((promise) => axios.get(promise))).then(
      axios.spread((posts, comments, reactions) => {
        //console.log({ posts, comments, reactions });
        setPostArray(posts.data);
        setCommentsArray(comments.data);
        setReactionsArray(reactions.data);
      })
    ).catch((err)=>{
      console.log(err);
    }).finally(()=>{
      setIsLoaded(true);
    });
  };

  useEffect(()=>{
    loadExplore();
    fetchData();
    getPosts();
  },[]);

  useEffect(()=>{
    getPosts();
  },[name,
    comment,  
    reactionToSend,
    reactionId,
    postArray,
    commentsArray,
    reactionsArray,
    yesDelete,
  toDelete])
 

  useEffect(() => {
    let abortController = new AbortController();
    
    fetchData();
    
    getPosts();
    return () => {
      abortController.abort();
    };
  }, [toggle
  ]);

  return (
    <div>
      <div className="TopBar">
      <div className="navBarNoCollapse">
        <img src={logo} alt="Logo" className="Logo" />
        <nav>
          <button className="btnupload2" onClick={home}>
            <MdOutlineHome />
          </button>
          <button className="noClick">|</button>
          <button className="btnupload2" onClick={imageUpload}>
            <MdOutlineFileUpload />
          </button>
        </nav>
        <button className="logout" onClick={logOut}>
          <MdLogout />
        </button>
        </div>
        <div className="navBarCollapsed">
          <img src={logo} alt="Logo" className="LogoCollapsed" />
          <nav>
          <button className="menuCollapsed" onClick={home}>
            <MdOutlineHome />
          </button>
          
          
          <button className="logoutCollapsed" onClick={logOut}>
          <MdLogout />
          </button>
        
        
          </nav>
        </div>
      </div>
      <div className="Home">
        <ul >
          <li>
            <form>
              <div className="input_div">
                <input
                  onChange={handleName}
                  className="input"
                  value={name}
                  type="name"
                />

                <button onClick={(e)=>{
                  e.preventDefault();
                }} style={{display: "none"}} type="submit">
                  Find
              </button>
              </div>
            </form>
          </li>
          {isLoaded ? (
          <div>
            {posts}
            <div className="modal" style={{display: modal? "block":"none"}}>
            <div className="modalContent" >
              <h4>Are you sure you want to delete this post?<br></br>If yes click on delete button again</h4>
              <button className="yesButton"  onClick={()=>{setModal(false)}}>OK</button>
            </div>
            </div>
          </div>
          
        ) : (
          <div>
            <p>Loading..</p>
          </div>
        )}
          
        </ul>
      </div>
    </div>
  );
}

export default Explore;
