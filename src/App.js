import Form from "./Form";
import Login from "./Login";
import Image from "./Image";
import React from "react";
import Explore from "./Explore";
import Profile from "./Profile";
import Password from "./PasswordReset";

//import { useNavigate } from "react-router-dom";
// import React, { Component }  from 'react';
import "./App.css";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import Home from "./Home";

function App() {
  
  // let navigate = useNavigate(); 
  // const loginChange = () =>{ 
  //   let path = `/login`; 
  //   navigate(path);
  // }

  return (
    
    <div className="App">
      <BrowserRouter>
        <Routes>
          <Route path="/login" element={<Login />} />
          <Route path="/register" element={<Form />} />
          <Route path="/image" element={<Image />} />
          <Route path="/homePage" element={<Home />} />
          <Route path="/explore" element={<Explore />} />
          <Route path="/profile" element={<Profile />} />
          <Route path="/forgotenPassword" element={<Password />} />
          <Route path="/" element={<Home />} />
        </Routes>
      </BrowserRouter>

    </div>
    
  );
}

export default App;
