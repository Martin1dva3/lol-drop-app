import { useState } from "react";
import React from "react";
import { useNavigate } from "react-router-dom";
import logo from "./logo22.png";
import axios from "axios";

function PasswordReset() {
  const baseURL = "https://lol-drop.herokuapp.com/rest-auth/password/reset/";

  const [email, setEmail] = useState("");

  const [submitted, setSubmitted] = useState(false);
  const [error, setError] = useState(false);
  const [response, setResponse] = useState("");
  const [errors, setErrors] = useState([]);

  let navigate = useNavigate();
  const routeChange = () => {
    let path = `/login`;
    navigate(path);
  };

  

  const handleEmail = (e) => {
    setEmail(e.target.value);
    setSubmitted(false);
    document.getElementById("email").style.border = "1px solid #37C8AB";
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    //console.log({ email: email });

    delete axios.defaults.headers.common["Authorization"];
    axios
      .post(baseURL, {
        email: email,
      })
      .then((response) => {
        setResponse(response.data);
        setSubmitted(true);
        setError(false);
        routeChange();
      })
      .catch((error) => {
        var errors = [];
        var result = error.response.data;
        console.log("JSON error ->", result);

        if (result.email) {
          setError(true);
          errors.push(JSON.stringify(result.email));
          console.log("json response email:" + JSON.stringify(result.email));
          document.getElementById("email").style.border = "1px solid #830000";
        }
        if (result.non_field_errors) {
          setError(true);
          errors.push(JSON.stringify(result.non_field_errors));
          console.log(
            "json response other:" + JSON.stringify(result.non_field_errors)
          );
        }

        setErrors(errors);
        console.log("error ->", error);
      });
  };

  const successMessage = () => {
    var result = JSON.stringify(response);

    return (
      <div
        className="success"
        style={{
          display: submitted ? "" : "none",
        }}
      >
        <h3>{result}</h3>
      </div>
    );
  };

  // Showing error message if error is true
  const errorMessage = () => {
    var results = [];
    for (var i = 0; i < errors.length; i++) {
      var temp = errors[i].toString();
      results.push(<p>{temp.substring(2, temp.length - 2)}</p>);
    }
    return (
      <div
        className="error"
        style={{
          display: error ? "" : "none",
        }}
      >
        {results}
      </div>
    );
  };

  return (
    <div>
    <img src={logo} alt="Logo" className="LogoLogin" />
    <div className="form">
      <div className="messages">
        {errorMessage()}
        {successMessage()}
      </div>
      <form>
        <div className="input_div">
          <label className="label">Email</label>
          <input
            id="email"
            onChange={handleEmail}
            className="input"
            value={email}
            type="email"
          />
        </div>
        <button onClick={handleSubmit} className="btn" type="submit">
          Submit
        </button>
      </form>
      <button onClick={routeChange} className="btn">
        Login
      </button>
    </div>
    </div>
  );
}
export default PasswordReset;
