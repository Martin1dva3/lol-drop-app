import { useState } from "react";
import React from "react";
import { useNavigate } from "react-router-dom";
import { useStateIfMounted } from "use-state-if-mounted";
import logo from "./logo22.png";
import qr from "./qr2.png";

import axios from "axios";

//const baseURL = "http://127.0.0.1:8000/rest-auth/login/";
const baseURL = "https://lol-drop.herokuapp.com/rest-auth/login/";

export default function Login() {
  let navigate = useNavigate();
  const routeChange = () => {
    let path = `/register`;
    navigate(path);
  };

  const homePage = () => {
    let path = `/homePage`;
    navigate(path);
  };

  // States for registration
  const [name, setName] = useStateIfMounted("");
  const [password, setPassword] = useStateIfMounted("");

  // States for checking the errors
  const [submitted, setSubmitted] = useState(false);
  const [error, setError] = useState(false);
  const [errors, setErrors] = useState([]);  

  // Handling the name change
  const handleName = (e) => {
    setName(e.target.value);
    setSubmitted(false);
  };

  // Handling the email change
  // Handling the password change
  const handlePassword = (e) => {
    setPassword(e.target.value);
    setSubmitted(false);
  };

  // Handling the form submission
  const handleSubmit = (e) => {
    e.preventDefault();
    if (name === "" || password === "") {
      document.getElementById("username").style.border = "1px solid #830000";
      document.getElementById("password").style.border = "1px solid #830000";
      setError(true);
    } else {
      /*console.log({
        username: name,
        password: password,
      });*/
      delete axios.defaults.headers.common["Authorization"];
      axios
        .post(baseURL, {
          username: name,
          password: password,
        })
        .then((response) => {
          sessionStorage.setItem("user_token", response.data.key);
          axios.defaults.headers.common[
            "Authorization"
          ] = `Token ${response.data.key}`;
          homePage();
          
          setSubmitted(true);
          setError(false);
        })
        .catch((error) => {
          document.getElementById("username").style.border =
            "1px solid #830000";
          document.getElementById("password").style.border =
            "1px solid #830000";
          var errors = [];
          var result = error.response.data;
          console.log("JSON error ->", result);
          if (result.password1) {
            setError(true);
            errors.push(JSON.stringify(result.password1));
            console.log(
              "json response password:" + JSON.stringify(result.password1)
            );
          }
          if (result.username) {
            
            setError(true);
            errors.push(JSON.stringify(result.username));
            console.log(
              "json response username:" + JSON.stringify(result.username)
            );
          }
          if (result.email) {
            setError(true);
            errors.push(JSON.stringify(result.email));
            console.log("json response email:" + JSON.stringify(result.email));
          }
          if (result.non_field_errors) {
            setError(true);
            errors.push(JSON.stringify(result.non_field_errors));
            console.log(
              "json response other:" + JSON.stringify(result.non_field_errors)
            );
          }

          setErrors(errors);
          console.log("error ->", error);
        });
    }
  };

  // Showing success message
  const successMessage = () => {
    
    return (
      <div
        className="success"
        style={{
          display: submitted ? "" : "none",
        }}
      >
        <h3>Loading</h3>
      </div>
    );
  };

  // Showing error message if error is true
  const errorMessage = () => {
    var results = [];
    for (var i = 0; i < errors.length; i++) {
      var temp = errors[i].toString();
      results.push(<p>{temp.substring(2, temp.length - 2)}</p>);
    }
    return (
      <div
        className="error"
        style={{
          display: error ? "" : "none",
        }}
      >
        <h3>
          {results.length === 0 ? "Please enter all the fields" : results}
        </h3>
      </div>
    );
  };

  return (
    <div>
    <img src={logo} alt="Logo" className="LogoLogin" />
    <div className="form">
      
      {/* <div>
      <img src={logo} alt="Logo" className='Logo'/>
      <h1>User Registration</h1>
    </div> */}

      {/* Calling to the methods */}
      <div className="messages">
        {errorMessage()}
        {successMessage()}
      </div>

      <form>
        {/* Labels and inputs for form data */}
        {/* <div className='input_div'>
          <label for="name" className="label">Name</label>
          <input id="name" onChange={handleName} className="input"
            value={name} type="text" />
        </div> */}
        <div className="input_div">
          <label className="label">User</label>
          <input
            id="username"
            onChange={handleName}
            className="input"
            value={name}
            type="name"
          />
        </div>
        <div className="input_div">
          <label className="label">Password</label>
          <input
            id="password"
            onChange={handlePassword}
            className="input"
            value={password}
            type="password"
          />
        </div>
        <div className="input_div">
          <button onClick={handleSubmit} className="btn" type="submit">
            Login
          </button>
        </div>
      </form>

      <div className="input_div">
        <button onClick={routeChange} className="btn">
          Registration
        </button>
      </div>
      <div className="input_div">
        <a href="/forgotenPassword">password forgotten ?</a>
      </div>
      <div>
        <img src={qr} alt="qr" className="QRLogin" />
      </div>
    </div>
  </div>
  );
}
