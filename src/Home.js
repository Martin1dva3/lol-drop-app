import "./App";
import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import axios from "axios";
import logo from "./logo22.png";
import Moment from "moment";
import {
  MdMoreVert,
  MdDelete,
  MdDownload,
  MdThumbUp,
  MdThumbDown,
  MdLogout,
  MdImageSearch,
  MdOutlineFileUpload,
  MdOutlineDashboard,
  MdOutlineImage,
} from "react-icons/md";
import { saveAs } from "file-saver";

const baseURL = "https://lol-drop.herokuapp.com/";

function Home() {
  const [modal, setModal] = useState(false);
  const [yesDelete, setYesDelete] = useState(false);
  const [toggle, setToggle] = useState(false);
  const [didMount, setDidMount] = useState(false);
  const [isLoaded, setIsLoaded] = useState(false);
  const [postArray, setPostArray] = useState([]);
  const [commentsArray, setCommentsArray] = useState([]);
  const [reactionsArray, setReactionsArray] = useState([]);
  const [name, setName] = useState("");
  var toDelete = 0;
  const [memes, setMemes] = useState([]);
  const [templates, setTemplates] = useState([]);
  const [showMemes, setShowMemes] = useState(true);
  const [comment, setComment] = useState("");
  var postid = 0;
  var reactionToSend = 0;
  var reactionId = 0;
  let navigate = useNavigate();
  const showTemplates = () => {
    setShowMemes(!showMemes);
    window.scrollTo(0, 0);
  };

  const handleComment = (e) => {
    setComment(e.target.value);
    //console.log(comment);
  };

  const handleSubmit = (e) => {
    //console.log("ahoj");
    /* console.log({
      date: new Date(),
      text: comment,
      post: postid,
      ownerid: sessionStorage.getItem("user_pk"),
      owner: sessionStorage.getItem("user_name"),
    });*/

    axios.defaults.headers.common[
      "Authorization"
    ] = `Token ${sessionStorage.getItem("user_token")}`;
    axios
      .post(baseURL + "api/comments/", {
        date: new Date(),
        text: comment,
        post: postid,
        ownerid: sessionStorage.getItem("user_pk"),
        owner: sessionStorage.getItem("user_name"),
      })
      .then((response) => {
        // console.log(response);
        setComment("");

        setToggle(!toggle);
      })
      .catch((err) => {
        console.log(err.response.data);
      });
    setToggle(!toggle);
  };

  const sendReaction = () => {
    //console.log("ahoj");
    /* console.log({
      date: new Date(),
      reaction: reactionToSend,
      postid: postid,
      ownerid: sessionStorage.getItem("user_pk"),
      owner: sessionStorage.getItem("user_name"),
    });*/

    axios.defaults.headers.common[
      "Authorization"
    ] = `Token ${sessionStorage.getItem("user_token")}`;
    axios
      .post(baseURL + "api/reactions/", {
        date: new Date(),
        reaction: reactionToSend,
        postid: postid,
        ownerid: sessionStorage.getItem("user_pk"),
        owner: sessionStorage.getItem("user_name"),
      })
      .then((response) => {
        //console.log(response);
        setComment("");
      })
      .catch((err) => {
        console.log(err.response.data);
      });
    setToggle(!toggle);
  };
  const updateReaction = () => {
    //console.log("ahoj");
    /*console.log({
      date: new Date(),
      reaction: reactionToSend,
      postid: postid,
      ownerid: sessionStorage.getItem("user_pk"),
      owner: sessionStorage.getItem("user_name"),
    });*/

    axios.defaults.headers.common[
      "Authorization"
    ] = `Token ${sessionStorage.getItem("user_token")}`;
    axios
      .patch(baseURL + `api/reactions/${reactionId}/`, {
        date: new Date(),
        reaction: reactionToSend,
        postid: postid,
        ownerid: sessionStorage.getItem("user_pk"),
        owner: sessionStorage.getItem("user_name"),
      })
      .then((response) => {
        //console.log(response);
        setToggle(!toggle);
      })
      .catch((err) => {
        console.log(err.response.data);
      });
    setToggle(!toggle);
  };

  const Delete = () => {
    //console.log("delete")
    setModal(false);
    setIsLoaded(false);
    axios.defaults.headers.common[
      "Authorization"
    ] = `Token ${sessionStorage.getItem("user_token")}`;
    axios
      .delete(baseURL + `api/posts/${toDelete}`)
      .then((response) => {
        // console.log(response);
        setToggle(!toggle);
      })
      .catch((err) => {
        console.log(err.response);
      });
    setYesDelete(false);
  };

  const profile = () => {
    let path = `/profile`;
    navigate(path);
  };

  const imageUpload = () => {
    let path = `/image`;
    navigate(path);
  };

  const login = () => {
    let path = `/login`;
    navigate(path);
  };

  const explore = () => {
    window.scrollTo(0, 0);
    let path = `/explore`;
    navigate(path);
  };

  const logOut = () => {
    axios.defaults.headers.common[
      "Authorization"
    ] = `Token ${sessionStorage.getItem("user_token")}`;
    axios.post(baseURL + "rest-auth/logout/").then((response) => {
      //console.log(response);
      delete axios.defaults.headers.common["Authorization"];
      sessionStorage.removeItem("user_token");
      login();
    });
  };

  const load = async () => {
    var result = "";
    if (!sessionStorage.getItem("user_token")) {
      setIsLoaded(false);
      login();
    }
    axios.defaults.headers.common[
      "Authorization"
    ] = `Token ${sessionStorage.getItem("user_token")}`;
    try {
      result = await axios.get(baseURL + "rest-auth/user/");
      const pk = await result.data.pk;
      const name = await result.data.username;
      const email = await result.data.email;
      sessionStorage.setItem("user_pk", pk);
      sessionStorage.setItem("user_name", name);
      sessionStorage.setItem("user_email", email);
    } catch (err) {
      setIsLoaded(false);
      console.log(err);
      login();
    } finally {
      setIsLoaded(true);
      setName(sessionStorage.getItem("user_name"));
    }

    //console.log(isLoaded);
  };

  const posts = () => {
    //console.log(postArray);

    var meme = [];
    var template = [];
    postArray.forEach((post) => {
      //console.log(post.ownerid);
      var comms = [];
      var postowner = "";
      if (post.ownerid.toString() === sessionStorage.getItem("user_pk")) {
        postowner = sessionStorage.getItem("user_name");
      } else {
        postowner = post.owner;
      }
      commentsArray.forEach((com) => {
        //console.log(com);
        if (com.post === post.id) {
          comms.push(
            <li key={com.id}>
              <div>
                <div className="commentOwner">
                  <b>
                    <p>{com.owner}</p>
                  </b>
                </div>
                <p className="commentText">{com.text}</p>
              </div>
            </li>
          );
        }
      });
      //console.log(reactionsArray);
      var likes = "";
      var dislikes = "";
      var a = 0;
      var b = 0;
      var liked = [];
      var disliked = [];
      var reacted = [];
      var reactionID = 0;
      reactionsArray.forEach((reac) => {
        if (reac.postid === post.id) {
          if (!reacted.includes(reac.ownerid.toString())) {
            reacted.push(reac.ownerid.toString());
            
            if (reac.reaction === 1) {
              liked.push(reac.ownerid.toString());
              a = a + 1;
            } else {
              disliked.push(reac.ownerid.toString());
              b = b + 1;
            }
            if (reac.ownerid.toString() === sessionStorage.getItem("user_pk")) {
              reactionID = reac.id;
              //console.log(reac.id)
            }
          }
        }

        //console.log(a);

        likes = a === 0 ? "" : a.toString();

        dislikes = b === 0 ? "" : b.toString();
      });

      if (!post.template) {
        meme.push(
          <li key={post.id}>
            <div className="Post">
              <div className="header">
                <div className="options">
                  <button className="optionsButton">
                    <MdMoreVert />
                  </button>
                  <div className="optionsDrop">
                    <button
                      style={{
                        display:
                          sessionStorage.getItem("user_pk") ===
                          post.ownerid.toString()
                            ? "block"
                            : "none",
                      }}
                      onClick={function () {
                        toDelete = post.id;
                        setModal(true);
                        setYesDelete(true);
                        if (yesDelete) {
                          Delete();
                        }
                      }}
                    >
                      <MdDelete />
                    </button>
                    <button
                      onClick={function () {
                        saveAs(post.media.toString());
                      }}
                    >
                      <MdDownload />
                    </button>
                  </div>
                </div>
                <h4>{postowner}</h4>
                <p className="datum">
                  Posted: {Moment(post.date).format("DD.MM.YYYY")}
                </p>
              </div>
              <img src={post.media} alt={post.owner + " meme"}></img>
              <div className="commentSection">
                <div className="reactions">
                  <button
                    className={
                      liked.includes(sessionStorage.getItem("user_pk"))
                        ? "reactionButton like liked"
                        : "reactionButton like"
                    }
                    onClick={function () {
                      postid = post.id;
                      reactionToSend = 1;
                      //console.log(sessionStorage.getItem("user_pk"));
                      if (
                        !reacted.includes(sessionStorage.getItem("user_pk"))
                      ) {
                        sendReaction();
                        setToggle(!toggle);
                      } else {
                        reactionId = reactionID;
                        updateReaction();
                        setToggle(!toggle);
                      }
                    }}
                  >
                    <MdThumbUp />
                    {likes}
                  </button>
                  <button
                    className={
                      disliked.includes(sessionStorage.getItem("user_pk"))
                        ? "reactionButton disLike disLiked"
                        : "reactionButton disLike"
                    }
                    onClick={function () {
                      postid = post.id;
                      reactionToSend = 0;
                      if (
                        !reacted.includes(sessionStorage.getItem("user_pk"))
                      ) {
                        sendReaction();
                        setToggle(!toggle);
                      } else {
                        reactionId = reactionID;
                        updateReaction();
                        setToggle(!toggle);
                      }
                    }}
                  >
                    <MdThumbDown />
                    {dislikes}
                  </button>
                </div>
                <div className="commentDrop">
                  <form>
                    <input
                      placeholder="Leave comment"
                      className="commentInput"
                      value={comment}
                      type="text"
                      onChange={handleComment}
                    />
                    <button
                      className="commentDropButton"
                      onClick={(e) => {
                        setToggle(!toggle);
                        postid = post.id;
                        //console.log({ id: postid });
                        e.preventDefault();
                        handleSubmit();
                      }}
                      type="submit"
                    >
                      Send
                    </button>
                  </form>
                  <div className="comments">
                    <ul>{comms}</ul>
                  </div>
                </div>
              </div>
            </div>
          </li>
        );
      } else {
        template.push(
          <li key={post.id}>
            <div className="Post">
              <div className="header">
                <div className="options">
                  <button className="optionsButton">
                    <MdMoreVert />
                  </button>
                  <div className="optionsDrop">
                    <button
                      style={{
                        display:
                          sessionStorage.getItem("user_pk") ===
                          post.ownerid.toString()
                            ? "block"
                            : "none",
                      }}
                      onClick={function () {
                        toDelete = post.id;
                        setModal(true);
                        setYesDelete(true);
                        if (yesDelete) {
                          Delete();
                        }
                      }}
                    >
                      <MdDelete />
                    </button>
                    <button
                      onClick={function () {
                        saveAs(post.media.toString());
                      }}
                    >
                      <MdDownload />
                    </button>
                  </div>
                </div>
                <h4>{post.owner}</h4>
                <p className="datum">
                  Posted: {Moment(post.date).format("DD.MM.YYYY")}
                </p>
              </div>
              <img src={post.media} alt={post.owner + " meme"}></img>
            </div>
          </li>
        );
      }
    });
    if (template.length === 0) {
      template.push(
        <li key="empty template list">
          <div className="PostEmpty">
            <div className="header">
              <h4>This list is empty</h4>
            </div>
          </div>
        </li>
      );
    }
    if (meme.length === 0 && postArray.length === 0) {
      meme.push(
        <li key="empty meme list">
          <div className="PostEmpty">
            <div className="header">
              <h4>This list is empty</h4>
            </div>
          </div>
        </li>
      );
    }
    //console.log("post");
    setMemes(meme);
    setTemplates(template);
  };

  const fetchData = () => {
    axios.defaults.headers.common[
      "Authorization"
    ] = `Token ${sessionStorage.getItem("user_token")}`;

    let endpoints = [
      baseURL + "api/posts/",
      baseURL + "api/comments/",
      baseURL + "api/reactions/",
    ];
    //console.log("fetch");
    axios
      .all(endpoints.map((promise) => axios.get(promise)))
      .then(
        axios.spread((posts, comments, reactions) => {
          //console.log({ posts, comments, reactions });
          setPostArray(posts.data);
          setCommentsArray(comments.data);
          setReactionsArray(reactions.data);
        })
      )
      .catch((err) => {
        console.log(err);
      })
      .finally(() => {
        setIsLoaded(true);
      });

    //setIsLoaded(true);
  };

  useEffect(() => {
    let abortController = new AbortController();
    setDidMount(true);
    //console.log("load");
    load();
    fetchData();

    return () => {
      setDidMount(false);
      setIsLoaded(false);
      abortController.abort();
    };
  }, []);

  useEffect(() => {
    posts();
  }, [
    comment,
    reactionToSend,
    reactionId,
    postArray,
    commentsArray,
    reactionsArray,
    showMemes,
    yesDelete,
    toDelete,
  ]);

  useEffect(() => {
    //console.log("useEffectFetch");
    fetchData();
  }, [toggle]);

  if (!didMount) {
    return null;
  }

  //clearInterval(interval);

  return (
    <div>
      <div className="TopBar">
        <div className="navBarNoCollapse">
          <img src={logo} alt="Logo" className="Logo" />
          <nav>
            <button
              className="btnTemplate"
              style={{ display: showMemes ? "inline-block" : "none" }}
              onClick={showTemplates}
            >
              <MdOutlineDashboard />
            </button>
            <button
              className="btnupload2"
              style={{ display: showMemes ? "none" : "inline-block" }}
              onClick={showTemplates}
            >
              <MdOutlineImage />
            </button>
            <button href="" className="noClick">
              |
            </button>
            <button className="btnmemes2" onClick={explore}>
              <MdImageSearch />
            </button>
            <button href="" className="noClick">
              |
            </button>
            <button className="btnupload2" href="" onClick={imageUpload}>
              <MdOutlineFileUpload />
            </button>
          </nav>
          <button className="profile" onClick={profile}>
            {name.charAt(0).toUpperCase()}
          </button>
          <button className="logout" onClick={logOut}>
            <MdLogout />
          </button>
        </div>

        <div className="navBarCollapsed">
          <img src={logo} alt="Logo" className="LogoCollapsed" />
          <nav>
            <button
              className="menuCollapsed"
              style={{ display: showMemes ? "" : "none" }}
              onClick={showTemplates}
            >
              <MdOutlineDashboard />
            </button>
            <button
              className="menuCollapsed"
              style={{ display: showMemes ? "none" : "" }}
              onClick={showTemplates}
            >
              <MdOutlineImage />
            </button>

            <button className="menuCollapsed" onClick={explore}>
              <MdImageSearch />
            </button>
            <button className="menuCollapsed" href="" onClick={imageUpload}>
              <MdOutlineFileUpload />
            </button>
            <button className="logoutCollapsed" onClick={logOut}>
              <MdLogout />
            </button>
            <button className="profileCollapsed" onClick={profile}>
              {name.charAt(0).toUpperCase()}
            </button>
          </nav>
        </div>
      </div>
      <div className="Home">
        {isLoaded ? (
          <div>
            <ul style={{ display: showMemes ? "block" : "none" }}>{memes}</ul>
            <ul style={{ display: showMemes ? "none" : "block" }}>
              {templates}
            </ul>
            <div
              className="modal"
              style={{ display: modal ? "block" : "none" }}
            >
              <div className="modalContent">
                <h4>
                  Are you sure you want to delete this post?<br></br>If yes
                  click on delete button again
                </h4>
                <button
                  className="yesButton"
                  onClick={() => {
                    setModal(false);
                  }}
                >
                  OK
                </button>
              </div>
            </div>
          </div>
        ) : (
          <div>
            <p>Loading..</p>
          </div>
        )}
      </div>
    </div>
  );
}

export default Home;
